const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const {check, validationResult} = require('express-validator');

const mongoose = require('mongoose');
mongoose.set("useFindAndModify", false);
mongoose.connect('mongodb://localhost:27017/CMS', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const fileupload = require('express-fileupload');

var session = require('express-session');
process.env.PWD = process.cwd();


const Page = mongoose.model('Page', {
    name: String,
    slug: String,
    content: String,
    user: String,
    image: String
});

const Login = mongoose.model('Login', {
    username: String,
    password: String
});

const Header = mongoose.model('Header',{
    tagline: String,
    logo: String,
    user: String
});

const slugRegex = /^[a-zA-Z0-9\$-\.\+!\*'\(\)_)]+$/;

var menuPages = [];

var header = new Header();

var myApp = express();

myApp.use(express.static(process.env.pwd + '/public'))

myApp.use(bodyParser.urlencoded({ extended:false}));

myApp.use(bodyParser.json())

myApp.set('views', path.join(__dirname, 'views'));
myApp.use(express.static(__dirname+'/public'));
myApp.set('view engine', 'ejs');
myApp.use(fileupload());
myApp.use(session({
    secret: 'mysecret',
    resave: true,
    saveUninitialized: true
}));

//---------------- Routes ------------------

myApp.get('/',function(req, res){
    getHeader();
    setMenu();
    Page.findOne().exec(function(err3, page)
    {
        if (err3) 
        {                    
            console.log("Page error " + err3);
        } 
        else 
        {
            console.log('header in get - ' + header);
            console.log('menu in get - ' + menuPages);
            if (page == null)
            {
                res.render('login', { 
                    header, menuPages
                });
            }
            else
            {
                res.render('displaypage', { 
                    header, page, menuPages
                });
            }
        }                        
    });  
});

myApp.get('/admin', function(req, res){
    setMenu();

    if (req.session.userLoggedIn){
        Page.find({}).exec(function(err, pages)
        {
            res.render('admin', {
                pages, menuPages, header
            });
        });
    }
    else 
    {
        getHeader();
        res.redirect('login');
    }
});

myApp.get('/login',function(req, res){
    if (req.session.userLoggedIn)
    {
        res.redirect('admin');
    }
    else
    {
        res.render('login', {menuPages, header});
    }    
});

myApp.post('/login', [
    check('username', 'Please enter your username').not().isEmpty(),
    check('password', 'Please enter your password').not().isEmpty(),
], function(req, res){
    var user = req.body.username;
    const errors = validationResult(req);
    if(!errors.isEmpty())
    {
        var errorsData = {
            errors: errors.array()
        };      
        res.render('login', {errorsData, menuPages, header});
    }
    else
    {
        var pass = req.body.password;
        Login.findOne({username:user, password:pass}).exec(function(err, foundUser)
        {
            if (err || foundUser == null)
            {
                var error = { msg: 'Username or password incorrect'};
                var errors = [];
                errors.push(error);
                var errorsData = { errors: errors };
                res.render('login', {errorsData, header, menuPages, user});
            }
            else
            {
                req.session.username = foundUser.username;
                req.session.userLoggedIn = true;
                req.session.save(function(err){});
                res.redirect('admin');
            }            
        });
    }
});

myApp.get('/logout',function(req, res){
    if (req.session.userLoggedIn)
    {
        req.session.username = '';
        req.session.userLoggedIn = false;
        req.session.save(function(err){});
        res.render('logout', {menuPages, header});        
    }
    else
    {
        res.redirect('login');
    }
});

myApp.get('/edit/:id',function(req, res){
    if (req.session.userLoggedIn)
    {
        var id = req.params.id;
        Page.findOne({_id:id}).exec(function(err, page)
        {
            res.render('edit', {page, header, menuPages})
        });
    } 
    else 
    {
        res.redirect('login');
    }    
});

myApp.post('/edit',[
    check('name', 'Please enter the page name').not().isEmpty(),
    check('slug', 'Please enter a valid slug').matches(slugRegex),
    check('content', 'Please enter some page content').not().isEmpty()
],function(req, res){
    var name = req.body.name;
    var content = req.body.content;
    var pageId = req.body.pageId;
    var slug = req.body.slug;
    var imageName = "";
    var image = "";
    var imagePath = "";
    var pageInfo = new Page({
        name: name,
        slug: slug,
        content: content,
        image: ''
    });

    if (req.files)
    {
        console.log("got here");
        imageName =  Date.now() + "-" + req.files.image.name;
        image = req.files.image;
        imagePath = "public/images/" + imageName;
        image.mv(imagePath);
        pageInfo.image = imageName;
    }
    else
    {
        pageInfo.image = req.body.oldImage;
    }

    const errors = validationResult(req);
    if(!errors.isEmpty())
    {
        var errorsData = {
            errors: errors.array()
        };      
        res.render('edit', {errorsData, header, menuPages, page:pageInfo});
    }
    else
    {
        Page.findOne({_id:pageId}).exec(function(err, page){
            page.slug = req.body.slug;
            page.name = req.body.name;
            page.content = req.body.content;
            page.image = pageInfo.image;
            page.save().then( ()=>{
                console.log('Page Saved');
            });
        });       
        setMenu();
        res.redirect('admin');
    }
});

myApp.get('/new',function(req, res){
    if (req.session.userLoggedIn)
    {
        setMenu();
        res.render('new', {header, menuPages});
    }    
    else 
    {
        res.redirect('login');
    }
});

myApp.post('/new',[
    check('name', 'Please enter the page name').not().isEmpty(),
    check('slug', 'Please enter a valid slug').matches(slugRegex),
    check('content', 'Please enter some page content').not().isEmpty()
],function(req, res){
    const errors = validationResult(req);
    var name = req.body.name;
    var content = req.body.content;
    var slug = req.body.slug;
    var user = req.session.username;
    var imageName = "";
    var image = "";
    var imagePath = "";
    var pageInfo = new Page({
        name: name,
        slug: slug,
        content: content,
        user: user,
        image: ''
    });


    if (req.files)
    {
        imageName =  Date.now() + "-" + req.files.image.name;
        image = req.files.image;
        imagePath = "public/images/" + imageName;
        image.mv(imagePath);
        pageInfo.image = imageName;
    }
    else
    {
        pageInfo.image = req.body.oldImage;
    }

    if(!errors.isEmpty())
    {
        var errorsData = {
            errors: errors.array()
        };
        var prevData = {
            name: name,
            slug: slug,
            content: content,
            image: imageName
        };
        res.render('new', {errorsData, prevData, header, menuPages});
    }
    else
    {
        pageInfo.save().then( ()=>{
            console.log('Page Created');
            menuPages.push({name:name, slug: 'pages/' + slug})
        });
        res.redirect('admin');
    }
});

myApp.get('/delete/:id',function(req, res){
    if (req.session.userLoggedIn)
    {
        var id = req.params.id;
        Page.findOneAndDelete({_id:id}).exec(function(err, page){
            var idx = menuPages.indexOf(page.name);
            if (idx !== -1) menuPages.splice(idx, 1);
            setMenu();
            res.redirect('../admin');
        });
    }
    else
    {
        res.redirect('login');
    }
});

myApp.post('/delete',function(req, res){
    res.redirect('admin');
});

myApp.get('/editheader', function(req, res){
    if (req.session.userLoggedIn)
    {
        setMenu();
        var user = req.session.username;   
        Header.findOne().exec(function(err, header){
            res.render('editheader', { header, menuPages });
        });
    }
    else
    {
        res.redirect('login');
    }    
});

myApp.post('/editheader',[
    check('tagline', 'Please enter the tagline').not().isEmpty()
],function(req, res){
    const errors = validationResult(req);

    var tagline = req.body.tagline;
    var user = req.body.userId;
    var imageName = "";
    var image = "";
    var imagePath = "";
    

    var header = new Header({user:user});
    if (req.body.tagline)
    {
        header.tagline = tagline;
    }
    
    if (req.files)
    {
        imageName =  Date.now() + "-" + req.files.logo.name;
        image = req.files.logo;
        imagePath = "public/images/" + imageName;
        image.mv(imagePath);
        header.logo = imageName;
    }
    else
    {
        header.logo = req.body.oldLogo;
    }

    if(!errors.isEmpty()){
        var errorsData = {
            errors: errors.array(),
            header: header
        }
        res.render('editheader', {header, menuPages, errorsData});
    }
    else
    {       
        Header.findOne().exec(function(err, foundHeader)
        {
            if (err) {
                console.log("Error: " + err);
            }
            foundHeader.tagline = header.tagline;
            foundHeader.logo = header.logo;
            foundHeader.user = header.user;
            foundHeader.save().then( ()=>{
                console.log('Header Saved');
                getHeader();
            });
        });
        res.redirect('admin');
    }
});


//-------- Last Route -------------

myApp.get('/pages/:slug',function(req, res){
    var slug = req.params.slug;
    setMenu();
    Page.findOne({slug:slug}).exec(function(err3, page)
    {
        if (err3) 
        {                    
            console.log("Page error " + err3);
        } 
        else 
        {
            res.render('displaypage', { 
                header, page, menuPages
            });
        }                        
    });  
});

// ------------- functions -------------

function setMenu()
{
    Page.find().exec(function(err2, pages)
    {   
        if (err2) 
        {
            console.log("Page error " + err2);
        } 
        else 
        {
            menuPages = new Array();
            menuPages.push({name:'Admin', slug:'../admin'});
            for(let i = 0; i < pages.length; i++)
            {
                var pName = pages[i].name;
                var pSlug = '/pages/' + pages[i].slug;
                menuPages.push({ name:pName, slug:pSlug });
            }
        }
    });
}

function getHeader()
{
    Header.findOne().exec(function(err1, foundHeader)
    {
        if (err1)
        {
            console.log("Header error " + err1);
        }
        else 
        {
            header = foundHeader;
        }        
    });
}


//----------- Start the server -------------------

myApp.listen(8080);
console.log('Server started at 8080 for CMS...');